﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Beacon))]
public class BeaconVisualState : MonoBehaviour
{
    public Transform[] RotationTransforms;
    public float RotationSpeed = 15.0f;

    private Beacon _b;

    private void Start()
    {
        _b = GetComponent<Beacon>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (_b.IsActivated)
        {
            for (int i = 0; i < RotationTransforms.Length; ++i)
            {
                RotationTransforms[i].Rotate(RotationTransforms[i].up, RotationSpeed * Time.deltaTime);
            }
        }
    }
}