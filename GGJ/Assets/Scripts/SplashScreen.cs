﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    public float delay = 1.0f;

    public Texture2D fadeTexture;
    private float fadeSpeed = 0.2f;
    private int drawDepth = -1000;

    private float alpha = 0.0f;

    private bool startFade = false;

    private void Start()
    {
        StartCoroutine(Fade(delay));
    }

    private IEnumerator Fade(float delay)
    {
        yield return new WaitForSeconds(delay);
        startFade = true;
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Texture2D.whiteTexture);
        if (startFade)
        {
            alpha += fadeSpeed * Time.deltaTime;
            alpha = Mathf.Clamp01(alpha);

            GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);

            GUI.depth = drawDepth;

            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);  
            if (alpha >= 1)
            {
                // Load next scene
                SceneManager.LoadScene(1);
            }
        }



    }
}