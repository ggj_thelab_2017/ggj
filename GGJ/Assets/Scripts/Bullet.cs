﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Queue<Vector3> BulletTransforms = new Queue<Vector3>();
    public Beacon BeaconRef = null;
    public float BulletSpeed { get; set; }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (BulletTransforms.Any())
        {
            Vector3 currTarget = BulletTransforms.Peek();
            if (currTarget != transform.position)
            {
                transform.position = Vector3.MoveTowards(transform.position, currTarget,
                    BulletSpeed * Time.fixedDeltaTime);
            }
            else
            {
                BulletTransforms.Dequeue();
                //Destroy(this);
                if (BulletTransforms.Count <= 0)
                {
                    BeaconRef.ToggleActivate(0);
                    Destroy(this.gameObject);
                }
                // Here reached end of path, check if beacon?

            }
        }
    }
}
