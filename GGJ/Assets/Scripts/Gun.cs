﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Laser))]
public class Gun : MonoBehaviour
{
	private Laser _laserBeam;

    public Transform Direction;
	public int StartColor = 0;

	// Use this for initialization
	void Start () {
        _laserBeam = GetComponent<Laser>();
	}


    public void Fire()
    {
        _laserBeam.Predict(Direction.position,Direction.forward,StartColor);
        //_laserBeam.Fire();
    }

	public void IncreaseColor()
	{
        //TODO: Boundary checks for our WaveVisualizer
	    WaveVisualiser v = GetComponent<WaveVisualiser>();
        if(v != null)v.IncreaseFrequency(this);

	    // WaveVisualiser.IncreaseFrequency
	}

	public void DecreaseColor()
	{
        //TODO: Boundary checks for our WaveVisualizer
	    WaveVisualiser v = GetComponent<WaveVisualiser>();
        if(v != null)v.DecreaseFrequency(this);
        // WaveVisualiser.DecreaseFrequency
	}
}
