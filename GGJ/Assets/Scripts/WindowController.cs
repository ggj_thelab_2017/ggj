﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowController : MonoBehaviour
{
    public bool SeeThrough = false;
    public int matId = 0;

    public Material TransparentMaterial;
    private Material _defaultMaterial;
    private Renderer r;


    private void Init()
    {
        
        if(!r) r = GetComponent<Renderer>();
        if(!_defaultMaterial) _defaultMaterial = r.material;
    }
    private void Start()
    {
        Init();
    }

    private void OnValidate()
    {
        Init();
        SetMaterial();
    }

    public void SetSeeThrough(bool b)
    {
        SeeThrough = b;
        SetMaterial();
    }
    public void SetMaterial()
    {
        if (SeeThrough && TransparentMaterial)
            r.material = TransparentMaterial;
        else
            r.material = _defaultMaterial;
    }

}
