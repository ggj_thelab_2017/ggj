﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR.InteractionSystem;

public class ButtonBehaviour : MonoBehaviour
{
    public Color SelectColor = Color.green;
    public Color NormalColor = Color.white;

    public UnityEvent OnClick;

    private void Start()
    {
    }




    private void OnValidate()
    {
        SetLookColor(NormalColor);
    }


    private void SetLookColor(Color color)
    {
       // var comp = GetComponentsInChildren<TextMesh>();
       // foreach (var c in comp)
       //     c.color = color;
       // var comp2 = GetComponentsInChildren<Renderer>();
       // foreach (var c in comp2)
       //     c.material.color = color;
    }
    public void OnLookEnter()
    {
       SetLookColor(SelectColor); 
    }

    public void OnLookExit()
    {
        SetLookColor(NormalColor);
    }


}
