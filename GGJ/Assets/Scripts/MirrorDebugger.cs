﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorDebugger : MonoBehaviour
{
	private Vector3 _startPos;

	private int _layerMirror;
	private int _layerWall;
	private int _layerEndPoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnDrawGizmos ()
	{
		_layerMirror = LayerMask.NameToLayer("Mirror");
		_layerWall = LayerMask.NameToLayer("Wall");
		_layerEndPoint = LayerMask.NameToLayer("EndPoint");

		_startPos = transform.position;
		CheckForNextPoint(_startPos, transform.forward);
	}

	private void CheckForNextPoint(Vector3 pos, Vector3 direc)
	{
		RaycastHit hit;
		if (Physics.Raycast(pos, direc, out hit, 100.0f))
		{
			int layerHit = hit.transform.gameObject.layer;

			if (layerHit == _layerWall)
			{
				Gizmos.DrawLine(pos, hit.point);
				return;
			}

			if (layerHit == _layerMirror)
			{
				Gizmos.DrawLine(pos, hit.point);

				Vector3 normal = hit.normal;
				float dot = Vector3.Dot(direc, normal);
				Vector3 reflectedDirec = direc - 2 * dot * normal;

				CheckForNextPoint(hit.point, reflectedDirec);
				return;
			}

			if (layerHit == _layerEndPoint)
			{
				Gizmos.DrawLine(pos, hit.point);
				return;
			}
		}

		Gizmos.DrawLine(pos, pos + direc * 100.0f);
	}
}
