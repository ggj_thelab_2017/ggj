﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunColorWheelVisualizer : MonoBehaviour
{

    public Material ColorWheelMaterial;

    public void SetColor(Color c)
    {
        ColorWheelMaterial.color = c;
        ColorWheelMaterial.SetColor("_EmissionColor",c);
    }
}
