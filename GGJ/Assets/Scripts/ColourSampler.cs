﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColourSampler : MonoBehaviour
{
    public Texture2D spectrum;
    public int steps;

    public Color GetColor(int number)
    {
        return spectrum.GetPixel((spectrum.width / steps) * number, 0);
    }

    // Plz don't judge. Quick fix
    public Queue<Color> GetColorRange(int number)
    {
        var l = new Queue<Color>();
        //if (number == 0)
        //{
        //    l.Enqueue(GetColor(number));
        //    return l;
        //}

        int firstPxX = (spectrum.width / steps) * number;
        int endPxX = (spectrum.width / steps) * (number + 1);

        for (int i = firstPxX; i < endPxX; ++i)
        {
            l.Enqueue(spectrum.GetPixel(i, 0));
        }
        return l;
    }

    public Queue<Color> GetColorRangeReversed(int number)
    {
        return new Queue<Color>(GetColorRange(number).Reverse());
    }
}