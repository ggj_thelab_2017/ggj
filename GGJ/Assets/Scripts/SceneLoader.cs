﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    public float FadeSpeed = 0.2f;
    public int DrawDepth = -1000;

    private bool bFade = false;
    private float alpha = 0.0f;


    private Texture2D _fadeTex;
    private void Awake()
    {
        _fadeTex = Texture2D.blackTexture;
    }

    public void LoadNextScene()
    {
        bFade = true;
    }
    private void OnGUI()
    {
        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _fadeTex);
        if (bFade)
        {
            
            alpha += FadeSpeed * Time.deltaTime;
            alpha = Mathf.Clamp01(alpha);
            GUI.depth = DrawDepth;

            if (alpha >= 1.0f)
            {
                
        // Load the scene
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
       



    }
}
