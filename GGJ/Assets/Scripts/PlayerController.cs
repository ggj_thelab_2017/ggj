﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
public class PlayerController : MonoBehaviour
{
    public float Force = 180.0f;
    public GameObject DeplayCameraObj;
    public GameObject CameraScreen;

    [Range(0,1)]
    public float DeadZone = 0.34f;

    // Used for properly shooting our ball
    public Transform RightGun;
    public Transform LeftGun;
    public AudioSource LaunchSound;

	// Use this for initialization
	void Start () {

	}
	
	void Update () {
        // Get your indices hmmmmmm
        SteamVR_ControllerManager m = GetComponent<SteamVR_ControllerManager>();
        SteamVR_TrackedObject toL = m.left.GetComponent<SteamVR_TrackedObject>();
        SteamVR_TrackedObject toR = m.right.GetComponent<SteamVR_TrackedObject>();
        int indexL = (int)toL.index;
	    int indexR = (int)toR.index;

        // Oh yeah baby
	    if (indexL <= -1 || indexR <= -1) return;

	    if (SteamVR_Controller.Input(indexR).GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
	    {
	        // Spawn camera object 
            if(DeplayCameraObj != null) DeplayCameraObj.SetActive(true);
	        if (DeplayCameraObj != null)
	        {
	            Rigidbody rb = DeplayCameraObj.GetComponent<Rigidbody>();
	            rb.velocity = Vector3.zero;
	            rb.isKinematic = false;
	            DeplayCameraObj.transform.position = RightGun.transform.position;
	            Vector3 shootV = RightGun.transform.forward + 0.2f * RightGun.transform.up;
	            rb.AddForce(shootV*Force,ForceMode.Impulse);
	            DeplayCameraObj.GetComponent<DeployableCam>().NextStick = false;
	        }

            if(LaunchSound)LaunchSound.Play();
	    }
	    if (SteamVR_Controller.Input(indexR).GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
	    {
	        if(DeplayCameraObj != null)DeplayCameraObj.GetComponent<DeployableCam>().NextStick = true;
	    }

	    // axis0 is touchpad position of ugly fat finger
	    if (SteamVR_Controller.Input(indexR).GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
	    {
	        CameraScreen.SetActive(!CameraScreen.activeSelf); 
	    }


	    Vector2 v = SteamVR_Controller.Input(indexL).GetAxis(EVRButtonId.k_EButton_Axis0);
	    Ray r = new Ray(LeftGun.transform.position, LeftGun.transform.forward);
	    RaycastHit info;

	    bool isHit = Physics.Raycast(r, out info, 100,LayerMask.GetMask("Mirror"));

        // Dispatch Events to looked at item
	    var input = SteamVR_Controller.Input(indexL);
	    if (input.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
	    {
	        if (isHit)
	        {
	            MirrorMovement mm = info.collider.gameObject.GetComponent<MirrorMovement>();
	            if (mm != null)
	            {
	                if((v - Vector2.left).magnitude < DeadZone)mm.TurnForward();
	                if((v - Vector2.right).magnitude < DeadZone)mm.TurnBackward();

	            }

	        }

	        Gun g = GetComponentInChildren<Gun>();
	        if (g != null)
	        {
	            
	            if ((v - Vector2.up).magnitude < DeadZone) g.IncreaseColor();
	            if ((v - Vector2.down).magnitude < DeadZone) g.DecreaseColor();
	        }
	    }
	    if (isHit && input.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
	    {
			var bb = info.collider.gameObject.GetComponent<ButtonBehaviour>();
            if(bb != null) bb.OnClick.Invoke();

	    }

	    if (input.GetPress(SteamVR_Controller.ButtonMask.Trigger))
	    {
	        Gun l = GetComponentInChildren<Gun>();
            if(l != null) l.Fire();
	    }
	    if (input.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
	    {
	        Laser l = GetComponentInChildren<Laser>();
	        if (l != null) l.SetSoundActive(true);
	    }
	    if (input.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
	    {
	        Laser l = GetComponentInChildren<Laser>();
	        if (l != null)
	        {
                l.SetSoundActive(false);
	            l.ResetLaser();
	        }
	    }
	}
}
